package test;

import main.Application;
import main.Database;
import main.Record;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class ApplicationTest {
    private ArrayList<Record> records = new ArrayList<>();

    private Predicate<Record> isClassTypeAB = (Record record) -> (record.getClassType() == "A" || record.getClassType() == "B" );
    private Predicate<Record> isClassTypeA = (Record record) -> (record.getClassType() == "A");
    private Predicate<Record> isCountryDEFR = (Record record) -> (record.getCountry() == "DE" || record.getCountry() == "FR");
    private Predicate<Record> priceGreaterOrEquals2 = (record -> (record.getPrice() >= 2));
    private Predicate<Record> customsfee0 = (record -> (record.getCustomFree() == 0));


    /**
     * Predicates
     */
    private Predicate<Record> countryDE = (record -> (record.getCountry() == "DE"));
    private Predicate<Record> countryDEOrFR = (record -> (record.getCountry() == "DE" || record.getCountry() == "FR" ));
    private Predicate<Record> countryDEOrES = (record -> (record.getCountry() == "DE" || record.getCountry() == "ES" ));
    private Predicate<Record> classTypeAOrC = (record -> (record.getClassType() == "A" || record.getClassType() == "C"));
    private Predicate<Record> classTypeAOrB = (record -> (record.getClassType() == "A" || record.getClassType() == "B"));
    private Predicate<Record> priceGreaterOrEquals3 = (record -> (record.getPrice() >= 3));
    private Predicate<Record> priceSmaller5 = (record -> (record.getPrice() < 5));

    @Before
    public void init() {
        Record record01 = new Record(1,"F1","DE","A",200.0,2.99,0.00);
        Record record02 = new Record(2,"F1","DE","C",150.0,3.99,0.00);
        Record record03 = new Record(3,"F2","FR","B",200.0,2.49,0.00);
        Record record04 = new Record(4,"F2","DE","C",200.0,2.49,0.00);
        Record record05 = new Record(5,"F3","ES","B",225.0,5.99,2.50);
        Record record06 = new Record(6,"F1","DE","B",150.0,3.99,0.00);
        Record record07 = new Record(7,"F3","ES","B",175.0,1.99,1.00);
        Record record08 = new Record(8,"F2","DE","B",100.0,3.49,2.00);
        Record record09 = new Record(9,"F1","ES","C",225.0,5.99,3.00);
        Record record10 = new Record(10,"F3","FR","B",150.0,1.49,0.00);

        records.add(record01);
        records.add(record02);
        records.add(record03);
        records.add(record04);
        records.add(record05);
        records.add(record06);
        records.add(record07);
        records.add(record08);
        records.add(record09);
        records.add(record10);
    }

    @Test
    public void testSQL01() {
        double averageWeight = records.stream().filter(countryDE)
                .filter(classTypeAOrC)
                .mapToDouble(record -> record.getWeight())
                .average()
                .getAsDouble();

        Assert.assertEquals(183.33333333333334, averageWeight, 2);
    }

    @Test
    public void testSQL02() {
        double maxPrice = records.stream()
                .filter(isClassTypeAB)
                .filter(isCountryDEFR)
                .mapToDouble(Record::getPrice)
                .max()
                .getAsDouble();

        Assert.assertEquals(3.99, maxPrice, 2);
    }

    @Test
    public void testSQL03() {
        List<Record> sortedByPriceRecords = records.stream().sorted(Comparator.comparing(Record::getPrice))
                .collect(Collectors.toList());

        String actual = "";
        for(Record record : sortedByPriceRecords) {
            actual += record.toString() + "  \n";
        }

        Assert.assertEquals("10 F3 FR B 150.0 1.49 0.0  \n" +
                "7 F3 ES B 175.0 1.99 1.0  \n" +
                "3 F2 FR B 200.0 2.49 0.0  \n" +
                "4 F2 DE C 200.0 2.49 0.0  \n" +
                "1 F1 DE A 200.0 2.99 0.0  \n" +
                "8 F2 DE B 100.0 3.49 2.0  \n" +
                "2 F1 DE C 150.0 3.99 0.0  \n" +
                "6 F1 DE B 150.0 3.99 0.0  \n" +
                "5 F3 ES B 225.0 5.99 2.5  \n" +
                "9 F1 ES C 225.0 5.99 3.0  \n", actual);
    }

    @Test
    public void testSQL04() {
        List<Record> list = records.stream()
                .sorted(Comparator.comparing(Record::getCountry).reversed()
                        .thenComparing(Record::getPrice).reversed())
                .collect(Collectors.toList());

        String actual = "";
        for(Record record : list) {
            actual += record.toString() + "  \n";
        }

        System.out.println(actual);

        Assert.assertEquals("2 F1 DE C 150.0 3.99 0.0  \n" +
                "6 F1 DE B 150.0 3.99 0.0  \n" +
                "8 F2 DE B 100.0 3.49 2.0  \n" +
                "1 F1 DE A 200.0 2.99 0.0  \n" +
                "4 F2 DE C 200.0 2.49 0.0  \n" +
                "5 F3 ES B 225.0 5.99 2.5  \n" +
                "9 F1 ES C 225.0 5.99 3.0  \n" +
                "7 F3 ES B 175.0 1.99 1.0  \n" +
                "3 F2 FR B 200.0 2.49 0.0  \n" +
                "10 F3 FR B 150.0 1.49 0.0  \n", actual);
    }

    @Test
    public void testSQL05() {
        List<Record> results = records.stream().filter(countryDEOrFR)
                .filter(classTypeAOrC)
                .filter(priceGreaterOrEquals3)
                .collect(Collectors.toList());

        String actual = "";
        for(Record record : results) {
            actual += record.toString() + "  \n";
        }

        Assert.assertEquals("2 F1 DE C 150.0 3.99 0.0  \n", actual);
    }

    @Test
    public void testSQL06() {
        List<Record> results = records.stream().filter(countryDEOrES)
                .filter(classTypeAOrB)
                .filter(priceSmaller5).sorted(Comparator.comparing(Record::getWeight).reversed())
                .collect(Collectors.toList());

        String actual = "";
        for(Record record : results) {
            actual += record.toString() + "  \n";
        }

        Assert.assertEquals("1 F1 DE A 200.0 2.99 0.0  \n" +
                "7 F3 ES B 175.0 1.99 1.0  \n" +
                "6 F1 DE B 150.0 3.99 0.0  \n" +
                "8 F2 DE B 100.0 3.49 2.0  \n", actual);
    }

    @Test
    public void testSQL07() {
        List<Record> results = records.stream().filter(countryDEOrES)
                .filter(classTypeAOrB)
                .filter(priceSmaller5)
                .sorted(Comparator.comparing(Record::getWeight).reversed())
                .limit(3)
                .collect(Collectors.toList());

        String actual = "";
        for(Record record : results) {
            actual += record.toString() + "  \n";
        }

        Assert.assertEquals("1 F1 DE A 200.0 2.99 0.0  \n" +
                "7 F3 ES B 175.0 1.99 1.0  \n" +
                "6 F1 DE B 150.0 3.99 0.0  \n", actual);
    }

    @Test
    public void testSQL08() {
        long counting = records.stream()
                .filter(countryDE)
                .filter(isClassTypeA)
                .filter(priceGreaterOrEquals2)
                .count();

        Assert.assertEquals(1, counting);
    }

    @Test
    public void testSQL09() {
        Map<String, Double> results = records.stream().collect(Collectors.groupingBy(Record::getCountry,Collectors.summingDouble(Record::getWeight)));

        String actual = "";
        for(Map.Entry<String, Double> entry : results.entrySet()) {
            actual += entry.getKey() + " " + entry.getValue() + "  \n";
        }

        Assert.assertEquals("DE 800.0  \n" +
                "FR 350.0  \n" +
                "ES 625.0  \n", actual);
    }

    @Test
    public void testSQL10() {
        Map<String, Long> result = records.stream()
                .filter(countryDEOrFR)
                .filter(customsfee0)
                .collect(Collectors.groupingBy(Record::getCountry,Collectors.counting()));

        String actual = "";
        for(Map.Entry<String, Long> entry : result.entrySet()) {
            actual += entry.getKey() + " " + entry.getValue() + "  \n";
        }

        Assert.assertEquals("DE 4  \n" +
                "FR 2  \n", actual);
    }
}