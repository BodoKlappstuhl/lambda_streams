package main;

import com.sun.org.apache.regexp.internal.RE;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Application implements IApplication {
    private ArrayList<Record> records;
    private Predicate<Record> isClassTypeAB = (Record record) -> (record.getClassType() == "A" || record.getClassType() == "B" );
    private Predicate<Record> isClassTypeA = (Record record) -> (record.getClassType() == "A");
    private Predicate<Record> isCountryDEFR = (Record record) -> (record.getCountry() == "DE" || record.getCountry() == "FR");
    private Predicate<Record> priceGreaterOrEquals2 = (record -> (record.getPrice() >= 2));
    private Predicate<Record> customsfee0 = (record -> (record.getCustomFree() == 0));


    /**
     * Predicates
     */
    private Predicate<Record> countryDE = (record -> (record.getCountry() == "DE"));
    private Predicate<Record> countryDEOrFR = (record -> (record.getCountry() == "DE" || record.getCountry() == "FR" ));
    private Predicate<Record> countryDEOrES = (record -> (record.getCountry() == "DE" || record.getCountry() == "ES" ));
    private Predicate<Record> classTypeAOrC = (record -> (record.getClassType() == "A" || record.getClassType() == "C"));
    private Predicate<Record> classTypeAOrB = (record -> (record.getClassType() == "A" || record.getClassType() == "B"));
    private Predicate<Record> priceGreaterOrEquals3 = (record -> (record.getPrice() >= 3));
    private Predicate<Record> priceSmaller5 = (record -> (record.getPrice() < 5));

    public Application() {
        records = new ArrayList<>();
    }

    public long convertDateStringToUnixSeconds(String dateString) {
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            return dateFormat.parse(dateString).getTime();
        } catch (ParseException pe) {
            System.out.println(pe.getMessage());
        }
        return -1;
    }

    public void init() {
        Record record01 = new Record(1,"F1","DE","A",200.0,2.99,0.00);
        Record record02 = new Record(2,"F1","DE","C",150.0,3.99,0.00);
        Record record03 = new Record(3,"F2","FR","B",200.0,2.49,0.00);
        Record record04 = new Record(4,"F2","DE","C",200.0,2.49,0.00);
        Record record05 = new Record(5,"F3","ES","B",225.0,5.99,2.50);
        Record record06 = new Record(6,"F1","DE","B",150.0,3.99,0.00);
        Record record07 = new Record(7,"F3","ES","B",175.0,1.99,1.00);
        Record record08 = new Record(8,"F2","DE","B",100.0,3.49,2.00);
        Record record09 = new Record(9,"F1","ES","C",225.0,5.99,3.00);
        Record record10 = new Record(10,"F3","FR","B",150.0,1.49,0.00);

        records.add(record01);
        records.add(record02);
        records.add(record03);
        records.add(record04);
        records.add(record05);
        records.add(record06);
        records.add(record07);
        records.add(record08);
        records.add(record09);
        records.add(record10);
    }

    public void printRecords() {
        records.stream().forEach(System.out::println);
    }

    public void executeQuery01() {
        System.out.println("--- SELECT AVG(weight) FROM data WHERE (country = 'DE' AND classType IN ('A','C'))");
        System.out.println();

        double averageWeight = records.stream().filter(countryDE)
                .filter(classTypeAOrC)
                .mapToDouble(record -> record.getWeight())
                .average()
                .getAsDouble();

        System.out.println(averageWeight);
    }

    public void executeQuery02() {
        double maxPrice;
        maxPrice = records.stream()
                .filter(isClassTypeAB)
                .filter(isCountryDEFR)
                .mapToDouble(Record::getPrice)
                .max()
                .getAsDouble();
        System.out.println("--- query02");
        System.out.println(maxPrice);
    }

    public void executeQuery03() {
        System.out.println("--- SELECT * FROM data ORDER BY price");
        System.out.println();

        List<Record> sortedByPriceRecords = records.stream().sorted(Comparator.comparing(Record::getPrice))
                .collect(Collectors.toList());

        System.out.println(sortedByPriceRecords);
        System.out.println();
    }

    public void executeQuery04() {
        // SELECT * FROM data ORDER BY country,price DESC
        List<Record> list = records.stream()
                .sorted(Comparator.comparing(Record::getCountry).thenComparing(Record::getPrice).reversed())
                .collect(Collectors.toList());
        System.out.println("--- query04");
        System.out.println(list);
    }

    public void executeQuery05() {
        System.out.println("--- SELECT * FROM data WHERE (country IN ('DE','FR') AND classType IN ('A','C') AND price >= 3)");
        System.out.println();

        List<Record> results = records.stream().filter(countryDEOrFR)
                .filter(classTypeAOrC)
                .filter(priceGreaterOrEquals3)
                .collect(Collectors.toList());

        results.forEach((result -> System.out.println(result)));
        System.out.println();
    }

    public void executeQuery06() {
        System.out.println("--- query06");
        System.out.println();

        List<Record> results = records.stream().filter(countryDEOrES)
                .filter(classTypeAOrB)
                .filter(priceSmaller5).sorted(Comparator.comparing(Record::getWeight).reversed())
                .collect(Collectors.toList());
        System.out.println(results);
    }

    public void executeQuery07() {
        System.out.println("--- SELECT * FROM data WHERE (country IN ('DE','ES') AND classType IN ('A','B') AND price < 5) ORDER BY weight DESC LIMIT 3");
        System.out.println();

        List<Record> results = records.stream().filter(countryDEOrES)
                .filter(classTypeAOrB)
                .filter(priceSmaller5)
                .sorted(Comparator.comparing(Record::getWeight).reversed())
                .limit(3)
                .collect(Collectors.toList());

        results.forEach((result -> System.out.println(result)));
        System.out.println();
    }

    public void executeQuery08() {
        System.out.println("--- query08");
        long counting = records.stream()
                .filter(countryDE)
                .filter(isClassTypeA)
                .filter(priceGreaterOrEquals2)
                .count();
        System.out.println(counting);
    }

    public void executeQuery09() {
        System.out.println("--- SELECT country,SUM(weight) FROM data GROUP BY country\n");
        System.out.println();

        Map<String, Double> results = records.stream().collect(Collectors.groupingBy(Record::getCountry,Collectors.summingDouble(Record::getWeight)));
        System.out.println(results);
    }

    public void executeQuery10() {
        System.out.println("--- query10");
        Map<String, Long> result = records.stream()
                .filter(countryDEOrFR)
                .filter(customsfee0)
                .collect(Collectors.groupingBy(Record::getCountry,Collectors.counting()));
        System.out.println(result);
    }

    public static void main(String... args) {
        Application application = new Application();
        application.init();
        application.printRecords();
        application.executeQuery01();
        application.executeQuery02();
        application.executeQuery03();
        application.executeQuery04();
        application.executeQuery05();
        application.executeQuery06();
        application.executeQuery07();
        application.executeQuery08();
        application.executeQuery09();
        application.executeQuery10();
    }
}