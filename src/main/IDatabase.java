package main;

import java.sql.ResultSet;

/**
 * Created by kevin on 23.02.2017.
 */
public interface IDatabase {
    void setupConnection();
    void getStatus();
    void update(String sqlStatement);
    void dropTable();
    void createTable();
    void dump(ResultSet resultSet);
    void queryDump(String sqlStatement);
    void init();
    void executeSQL01();
    void executeSQL02();
    void executeSQL03();
    void executeSQL04();
    void executeSQL05();
    void executeSQL06();
    void executeSQL07();
    void executeSQL08();
    void executeSQL09();
    void executeSQL10();
    void shutdown();
}
