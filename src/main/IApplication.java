package main;

public interface IApplication {
    void init();
    void printRecords();
    void executeQuery01();
    void executeQuery02();
    void executeQuery03();
    void executeQuery04();
    void executeQuery05();
    void executeQuery06();
    void executeQuery07();
    void executeQuery08();
    void executeQuery09();
    void executeQuery10();
}