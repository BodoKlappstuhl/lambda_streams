package main;

public class Record {
    private int id;
    private String name;
    private String country;
    private String classType;
    private double weight;
    private double price;
    private double customFree;


    public Record(int id, String name, String country, String classType, double weight, double price, double customFree) {
        this.id = id;
        this.name = name;
        this.country = country;
        this.classType = classType;
        this.weight = weight;
        this.price = price;
        this.customFree = customFree;
    }

    @Override
    public String toString() {
        return
                id +" " +
                name + " " +
                country +" " +
                classType +" " +
                weight +" " +
                price +" " +
                customFree;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getClassType() {
        return classType;
    }

    public void setClassType(String classType) {
        this.classType = classType;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getCustomFree() {
        return customFree;
    }

    public void setCustomFree(double customFree) {
        this.customFree = customFree;
    }
}