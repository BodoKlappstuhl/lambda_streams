import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

public class Application implements IApplication {
    private final String userDirectory = System.getProperty("user.dir");
    private final String fileSeparator = System.getProperty("file.separator");
    private final String dataDirectory = userDirectory + fileSeparator + "data" + fileSeparator;
    private final String databaseFile = dataDirectory + "exercise.db";

    private Connection connection;
    private String driverName = "jdbc:hsqldb:";
    private String username = "sa";
    private String password = "";

    public void setupConnection() {
        System.out.println("--- setupConnection");

        try {
            Class.forName("org.hsqldb.jdbcDriver");
            String databaseURL = driverName + databaseFile;
            connection = DriverManager.getConnection(databaseURL,username,password);
            System.out.println("connection : " + connection);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void getStatus() {
        try {
            System.out.println("--- status");
            System.out.println("userDirectory : " + userDirectory);
            System.out.println("dataDirectory : " + dataDirectory);
            System.out.println("databaseFile  : " + databaseFile);
            System.out.println("driverName    : " + driverName);
            System.out.println("username      : " + username);
            System.out.println("password      : " + password);
            System.out.println("connection    : " + connection.hashCode());
            System.out.println("schema        : " + connection.getSchema());
            System.out.println("status        : " + connection.isClosed());
            System.out.println();
        } catch (SQLException sqle) {
            System.out.println(sqle.getMessage());
        }
    }

    public synchronized void update(String sqlStatement) {
        try {
            Statement statement = connection.createStatement();
            int result = statement.executeUpdate(sqlStatement);
            if (result == -1)
                System.out.println("error executing " + sqlStatement);
            statement.close();
        } catch (SQLException sqle) {
            System.out.println(sqle.getMessage());
        }
    }

    public void dropTable() {
        System.out.println("--- dropTable");

        StringBuilder sqlStringBuilder = new StringBuilder();
        sqlStringBuilder.append("DROP TABLE data");
        System.out.println("sqlStringBuilder : " + sqlStringBuilder.toString());

        update(sqlStringBuilder.toString());
    }

    public void createTable() {
        System.out.println("--- createTable");

        StringBuilder sqlStringBuilder = new StringBuilder();
        sqlStringBuilder.append("CREATE TABLE data ").append(" ( ");
        sqlStringBuilder.append("id INTEGER NOT NULL").append(",");
        sqlStringBuilder.append("name VARCHAR(10) NOT NULL").append(",");
        sqlStringBuilder.append("country VARCHAR(2) NOT NULL").append(",");
        sqlStringBuilder.append("classType VARCHAR(1) NOT NULL").append(",");
        sqlStringBuilder.append("weight DOUBLE NOT NULL").append(",");
        sqlStringBuilder.append("price DOUBLE NOT NULL").append(",");
        sqlStringBuilder.append("customsFee DOUBLE NOT NULL").append(",");
        sqlStringBuilder.append("PRIMARY KEY (id)");
        sqlStringBuilder.append(" )");
        System.out.println("sqlStringBuilder : " + sqlStringBuilder.toString());

        update(sqlStringBuilder.toString());
    }

    public void dump(ResultSet resultSet) {
        try {
            ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
            int maximumNumberColumns = resultSetMetaData.getColumnCount();
            int i;
            Object object;

            for (;resultSet.next();) {
                for (i = 0;i < maximumNumberColumns;++i) {
                    object = resultSet.getObject(i + 1);
                    System.out.print(object.toString() + " ");
                }
                System.out.println(" ");
            }
        } catch (SQLException sqle) {
            System.out.println(sqle.getMessage());
        }
    }

    public synchronized void queryDump(String sqlStatement) {
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlStatement);
            dump(resultSet);
            statement.close();
        } catch (SQLException sqle) {
            System.out.println(sqle.getMessage());
        }
    }

    private String buildSQLStatement(int id,String name,String country,String classType,double weight,double price,double customsFee) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("INSERT INTO data (id,name,country,classType,weight,price,customsFee) VALUES (");
        stringBuilder.append(id).append(",");
        stringBuilder.append("'").append(name).append("'").append(",");
        stringBuilder.append("'").append(country).append("'").append(",");
        stringBuilder.append("'").append(classType).append("'").append(",");
        stringBuilder.append(weight).append(",");
        stringBuilder.append(price).append(",");
        stringBuilder.append(customsFee);
        stringBuilder.append(")");
        System.out.println(stringBuilder.toString());
        return stringBuilder.toString();
    }

    private void insertData() {
        update(buildSQLStatement(1,"F1","DE","A",200.0,2.99,0.00));
        update(buildSQLStatement(2,"F1","DE","C",150.0,3.99,0.00));
        update(buildSQLStatement(3,"F2","FR","B",200.0,2.49,0.00));
        update(buildSQLStatement(4,"F2","DE","C",200.0,2.49,0.00));
        update(buildSQLStatement(5,"F3","ES","B",225.0,5.99,2.50));
        update(buildSQLStatement(6,"F1","DE","B",150.0,3.99,0.00));
        update(buildSQLStatement(7,"F3","ES","B",175.0,1.99,1.00));
        update(buildSQLStatement(8,"F2","DE","B",100.0,3.49,2.00));
        update(buildSQLStatement(9,"F1","ES","C",225.0,5.99,3.00));
        update(buildSQLStatement(10,"F3","FR","B",150.0,1.49,0.00));
    }

    public void init() {
        insertData();
    }

    // aggregation - average
    public void executeSQL01() {
        System.out.println("--- executeSQL01");
        String sqlStatement = "SELECT AVG(weight) FROM data WHERE (country = 'DE' AND classType IN ('A','C'))";
        queryDump(sqlStatement);
        System.out.println();
    }

    // aggregation - max
    public void executeSQL02() {
        System.out.println("--- executeSQL02");
        String sqlStatement = "SELECT MAX(price) FROM data WHERE (country IN ('DE','FR') AND classType IN ('A','B'))";
        queryDump(sqlStatement);
        System.out.println();
    }

    // sort
    public void executeSQL03() {
        System.out.println("--- executeSQL03");
        String sqlStatement = "SELECT * FROM data ORDER BY price";
        queryDump(sqlStatement);
        System.out.println();
    }

    // sort
    public void executeSQL04() {
        System.out.println("--- executeSQL04");
        String sqlStatement = "SELECT * FROM data ORDER BY country,price DESC";
        queryDump(sqlStatement);
        System.out.println();
    }

    // filter
    public void executeSQL05() {
        System.out.println("--- executeSQL05");
        String sqlStatement = "SELECT * FROM data WHERE (country IN ('DE','FR') AND classType IN ('A','C') AND price >= 3)";
        queryDump(sqlStatement);
        System.out.println();
    }

    // filter and sort
    public void executeSQL06() {
        System.out.println("--- executeSQL06");
        String sqlStatement = "SELECT * FROM data WHERE (country IN ('DE','ES') AND classType IN ('A','B') AND price < 5) " +
                              "ORDER BY weight DESC";
        queryDump(sqlStatement);
        System.out.println();
    }

    // filter, sort and limit
    public void executeSQL07() {
        System.out.println("--- executeSQL07");
        String sqlStatement = "SELECT * FROM data WHERE (country IN ('DE','ES') AND classType IN ('A','B') AND price < 5) " +
                              "ORDER BY weight DESC LIMIT 3";
        queryDump(sqlStatement);
        System.out.println();
    }

    // aggregation - count
    public void executeSQL08() {
        System.out.println("--- executeSQL08");
        String sqlStatement = "SELECT COUNT(*) FROM data WHERE (country = 'DE' AND classType = 'A' AND price >= 2)";
        queryDump(sqlStatement);
        System.out.println();
    }

    // aggregation - group
    public void executeSQL09() {
        System.out.println("--- executeSQL09");
        String sqlStatement = "SELECT country,SUM(weight) FROM data GROUP BY country";
        queryDump(sqlStatement);
        System.out.println();
    }

    // aggregation - group and filter
    public void executeSQL10() {
        System.out.println("--- executeSQL10");
        String sqlStatement = "SELECT country,COUNT(*) FROM data WHERE (country IN ('DE','FR') AND customsFee = 0) " +
                              "GROUP BY country";
        queryDump(sqlStatement);
        System.out.println();
    }

    public void shutdown() {
        System.out.println("--- shutdown");

        try {
            Statement statement = connection.createStatement();
            statement.execute("SHUTDOWN");
            connection.close();
            System.out.println("isClosed : " + connection.isClosed());
        } catch (SQLException sqle) {
            System.out.println(sqle.getMessage());
        }
    }

    public static void main(String... args) {
        Application application = new Application();
        application.setupConnection();
        application.getStatus();
        application.dropTable();
        application.createTable();
        application.init();
        application.executeSQL01();
        application.executeSQL02();
        application.executeSQL03();
        application.executeSQL04();
        application.executeSQL05();
        application.executeSQL06();
        application.executeSQL07();
        application.executeSQL08();
        application.executeSQL09();
        application.executeSQL10();
        application.shutdown();
    }
}